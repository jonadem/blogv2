+++
title = "La Forge"
description = "My blog site."
sort_by = "date"
template = "blog.html"
page_template = "post.html"
insert_anchor_links = "none"
generate_feed = true

[extra]
lang = 'fr'
show_post_num = 10
metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Blog de Akanoa" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/home.png" },
]
+++